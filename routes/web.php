<?php

/** @var \Laravel\Lumen\Routing\Router $router */

use Illuminate\Support\Facades\DB as DB;

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});
$router->get('/cacheEducation', 'EducationController@index');
$router->get('/cacheGender', 'GenderController@index');
$router->get('/test', 'CategoryController@index');
// API route group
$router->group(['prefix' => 'api'], function () use ($router) {
    //User Routes
    $router->group(['prefix' => 'user'], function () use ($router) {
        // Matches "/api/user/register
        $router->post('register', 'AuthController@register');
        // Matches "/api/user/login
        $router->post('login', 'AuthController@login');
        // Matches "/api/user/profile
        $router->get('profile', 'UserController@profile');
        // Matches "/api/user/all
        $router->get('all', 'UserController@allUsers');
        // Matches "/api/user/1
        //get one user by id
        $router->get('{id}', 'UserController@singleUser');
        //Matches "/api/user/update/1"
        $router->post('update/{id}', 'UserController@updateUser');
        //Matches "/api/user/delete/1"
        $router->post('delete/{id}', 'UserController@deleteUser');
    });
    //Book Routes
    $router->group(['prefix' => 'book'], function () use ($router) {
        //Matches "api/book/create"
        $router->post('create', 'BookController@create');
        //Matches "api/book/all"
        $router->get('all', 'BookController@index');
        //Matches "api/book/1"
        $router->get('{id}', 'BookController@read');
        //Matches "api/book/update/1"
        $router->post('update/{id}', 'BookController@update');
        //Matches "api/book/delete/1"
        $router->post('delete/{id}', 'BookController@delete');
    });
});
// ADMIN route group
$router->group(['prefix' => 'admin', 'namespace' => 'admin'], function () use ($router) {
    $router->group(['prefix' => 'user'], function () use ($router) {
        // Matches "/admin/user/create"
        $router->post('create', 'AuthController@create');
        // Matches "/admin/user/profile"
        $router->get('profile', 'UserController@profile');
        // Matches "/admin/user/all"
        $router->get('all', 'UserController@allUsers');
        //Matches "/admin/user/1"
        $router->get('{id}', 'UserController@singleUser');
        //Matches "/admin/user/update/1"
        $router->post('update/{id}', 'UserController@updateUser');
        //Matches "/admin/user/delete/1"
        $router->post('delete/{id}', 'UserController@deleteUser');
    });
    //Writer Routes
    $router->group(['prefix' => 'writer'], function () use ($router) {
        //Matches "admin/writer/create"
        $router->post('create', 'WriterController@create');
        //Matches "admin/writer/all"
        $router->get('all', 'WriterController@index');
        //Matches "admin/writer/1"
        $router->get('{id}', 'WriterController@read');
        //Matches "admin/writer/update/1"
        $router->post('update/{id}', 'WriterController@update');
        //Matches "admin/writer/delete/1"
        $router->post('delete/{id}', 'WriterController@delete');
    });
    //Category Routes
    $router->group(['prefix' => 'category'], function () use ($router) {
        //Matches "admin/category/create"
        $router->post('create', 'CategoryController@create');
        //Matches "admin/category/all"
        $router->get('all', 'CategoryController@index');
        //Matches "admin/category/1"
        $router->get('{id}', 'CategoryController@read');
        //Matches "admin/category/update/1"
        $router->post('update/{id}', 'CategoryController@update');
        //Matches "admin/category/delete/1"
        $router->post('delete/{id}', 'CategoryController@delete');
    });
    //Book Routes
    $router->group(['prefix' => 'book'], function () use ($router) {
        //Matches "admin/book/create"
        $router->post('create', 'BookController@create');
        //Matches "admin/book/all"
        $router->get('all', 'BookController@index');
        //Matches "admin/book/1"
        $router->get('{id}', 'BookController@read');
        //Matches "admin/book/update/1"
        $router->post('update/{id}', 'BookController@update');
        //Matches "admin/book/delete/1"
        $router->post('delete/{id}', 'BookController@delete');
    });
});
