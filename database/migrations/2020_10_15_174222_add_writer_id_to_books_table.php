<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddWriterIdToBooksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('books', function (Blueprint $table) {
            $table->dropColumn('writer');
            $table->foreignId('writer_id')
            ->nullable()
            ->constrained('writers')
            ->onDelete('cascade');
            $table->boolean('confirmation')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('books', function (Blueprint $table) {
            $table->string('writer');
            $table->dropColumn('writer_id');
            $table->dropColumn('confirmation');
        });
    }
}
