<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;
class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection(env('DB_CONNECTION'))->create('users', function (Blueprint $table) {
            $table->id();
            $table->string('email')->unique();
            $table->string('password');
            $table->string('name');
            $table->string('lastname');
            $table->foreignId('education_id')
                ->nullable()
                ->constrained('education')
                ->onDelete('cascade');
            $table->string('gender');
            $table->integer('age');
            $table->string('avatar')->nullable();
            $table->timestamp('register_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('login_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->date('birth_date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
