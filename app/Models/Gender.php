<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Gender extends Model
{
    protected $fillable=['name'];
    protected $connection;

    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);
        $this->setConnection(env('DB_CONNECTION'));
    }
}
