<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Config;

class Education extends Model
{
    protected $connection;
    public $timestamps = true;
    protected $fillable = ['name','created_at','updated_at'];

    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);
        $this->setConnection(env('DB_CONNECTION'));
    }
}
