<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Config;
class Book extends Model
{
    protected $fillable = ['name','price','GeorgianYear','SolarYear','image','writer_id','confirmation'];
    protected $connection;

    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);
        $this->setConnection(env('DB_CONNECTION'));
    }
    public function categories()
    {
        $this->belongsToMany('App\Models\Category');
    }
    
}
