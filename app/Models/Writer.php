<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Writer extends Model
{
    protected $fillable = ['name','lastname'];
    protected $connection;

    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);
        $this->setConnection(env('DB_CONNECTION'));
    }
    public function books(){
        return $this->hasMany('App\Model\Book');
    }
}
