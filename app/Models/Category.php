<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Config;

class Category extends Model
{
    protected $fillable = ['name'];
    protected $connection;

    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);
        $this->setConnection(env('DB_CONNECTION'));
    }
    public function books()
    {
        $this->belongsToMany('App\Models\Book');
    }
}
