<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Config;

class Cache extends Model
{
    protected $connection;

    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);
        $this->setConnection(env('DB_CONNECTION'));
    }
}
