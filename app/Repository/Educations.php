<?php

namespace App\Repository;
use App\Models\Education;
use Carbon\Carbon;
use Illuminate\Support\Facades\Cache;

class Educations
{
    const CACHE_KEY = 'EDUCATIONS';

    public function all($orderBy)
    {
        $key = "all.$orderBy";
        $cacheKey = $this->getCacheKey($key);
        return Cache::remember($cacheKey, Carbon::now()->addMinutes(5), function () use($orderBy) {
            return Education::orderBy($orderBy,'desc')->get();
        });

    }
    public function getCacheKey($key)
    {
        $key = strtoupper($key);
        return self::CACHE_KEY . ".$key";
    }
}
