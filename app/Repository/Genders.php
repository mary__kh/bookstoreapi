<?php

namespace App\Repository;

use App\Models\Gender;
use Carbon\Carbon;
use Illuminate\Support\Facades\Cache;

class Genders
{
    const CACHE_KEY = 'GENDERS';

    public function all($orderBy)
    {
        $key = "all.$orderBy";
        $cacheKey = $this->getCacheKey($key);
        return Cache::remember($cacheKey, Carbon::now()->addMinutes(5), function () use($orderBy) {
            return Gender::orderBy($orderBy,'desc')->get();
        });

    }
    public function getCacheKey($key)
    {
        $key = strtoupper($key);
        return self::CACHE_KEY . ".$key";
    }
}
