<?php
namespace App\Http\Controllers\Traits;
use App\Libraries\Helpers;

trait ImageUpload
{

    /**
     * Image upload trait used in controllers to upload files
     */
    public function saveImages($file)
    {
        $destinationPath = '/uploads/';
        $file_name = time().'-'.$file->getClientOriginalName();
        $file->move(Helpers::public_path() . $destinationPath, $file_name);
        return 'uploads/'.$file_name;
    }

}
