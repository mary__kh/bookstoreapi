<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json(['categories' =>  Category::all()], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|min:3|max:255'
        ]);
        $data = $validator->validated();
        try {
            $category = Category::create($data);
            $category->save();
            //return successful response
            return response()->json(['category' => $category, 'message' => 'CREATED'], 201);
        } catch (\Exception $e) {
            //return error message
            return response()->json(['message' => 'Category Creation Failed!'], 409);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function read($id)
    {
        try {
            $category = Category::findOrFail($id);

            return response()->json(['category' => $category], 200);
        } catch (\Exception $e) {

            return response()->json(['message' => 'category not found!'], 404);
        }
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $category = Category::findOrFail($id);
        $validator = Validator::make($request->all(), [
            'name' => 'string|min:3|max:255',
        ]);
        $data = $validator->validated();
        try {
            $category->update($data);
            return response()->json(['message' => 'category updated successfully!'], 200);
        } catch (\Exception $e) {

            return response()->json(['message' => 'unfortunately category was not updated!'], 403);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        try {
            $category = Category::findOrFail($id);
            $category->delete();
            return response()->json(['message' => 'category successfully deleted!'], 200);
        } catch (\Exception $e) {
            return response()->json(['message' => 'Unfortunately category was not deleted!'], 400);
        }
    }
}
