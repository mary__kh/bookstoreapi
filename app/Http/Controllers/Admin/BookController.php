<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Book;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class BookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json(['books' =>  Book::all()], 200);
    }

    /**
     * Create a new resource
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|min:3|max:255|unique',
            'price' => 'required|numeric',
            'GeorgianYear' => 'required|numeric|max:4',
            'SolarYear' => 'required|numeric|max:4',
            'image' => 'required|image|mimes:jpeg,jpg,png,gif|max:100000',
            'writer_id' => 'required|numeric',
            'confirmation' => 'required|boolean'
        ]);
        $data = $validator->validated();
        $data['user_id'] = Auth::user()->id;
        if (!$data['confirmation']) {
            $data['published_at'] = null;
        }
        $data['published_at'] = Carbon::now();

        try {
            $book = Book::create($data);
            $book->save();
            //return successful response
            return response()->json(['book' => $book, 'message' => 'CREATED'], 201);
        } catch (\Exception $e) {
            //return error message
            return response()->json(['message' => 'Book Creation Failed!'], 409);
        }
    }

    /**
     * Read the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function read($id)
    {
        try {
            $book = Book::findOrFail($id);
            return response()->json(['book' => $book], 200);
        } catch (\Exception $e) {

            return response()->json(['message' => 'book not found!'], 404);
        }
    }

    /**
     * Update the specified resource in storage
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $book = Book::findOrFail($id);
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|min:3|max:255|unique',
            'price' => 'required|numeric',
            'GeorgianYear' => 'required|numeric|max:4',
            'SolarYear' => 'required|numeric|max:4',
            'image' => 'nullable|image|mimes:jpeg,jpg,png,gif|max:100000',
            'writer_id' => 'required|numeric',
            'confirmation' => 'required|boolean'
        ]);
        $data = $validator->validated();
        $data['user_id'] = Auth::user()->id;
        if ($data['confirmation']) {
            $data['published_at'] = Carbon::now();
        } else {
            $data['published_at'] = null;
        }
        try {
            $book->update($data);
            return response()->json(['message' => 'book updated successfully!'], 200);
        } catch (\Exception $e) {

            return response()->json(['message' => 'unfortunately book was not updated!'], 403);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        try {
            $book = Book::findOrFail($id);
            $book->delete();
            return response()->json(['message' => 'Book successfully deleted!'], 200);
        } catch (\Exception $e) {
            return response()->json(['message' => 'Unfortunately book was not deleted!'], 400);
        }
    }
}
