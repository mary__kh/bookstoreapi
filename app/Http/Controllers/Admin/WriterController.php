<?php

namespace App\Http\Controllers\Admin;

use App\Models\Writer;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class WriterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json(['writers' =>  Writer::all()], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|min:3|max:255',
            'lastname' => 'required|string|min:3|max:255'
        ]);
        $data = $validator->validated();
        try {
            $writer = Writer::create($data);
            $writer->save();
            //return successful response
            return response()->json(['writer' => $writer, 'message' => 'CREATED'], 201);
        } catch (\Exception $e) {
            //return error message
            return response()->json(['message' => 'Writer Creation Failed!'], 409);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function read($id)
    {
        try {
            $writer = Writer::findOrFail($id);

            return response()->json(['writer' => $writer], 200);
        } catch (\Exception $e) {

            return response()->json(['message' => 'writer not found!'], 404);
        }
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $writer = Writer::findOrFail($id);
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|min:3|max:255',
            'lastname' => 'required|string|min:3|max:255'
        ]);
        $data = $validator->validated();
        try {
            $writer->update($data);
            return response()->json(['message' => 'writer updated successfully!'], 200);
        } catch (\Exception $e) {

            return response()->json(['message' => 'unfortunately writer was not updated!'], 403);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        try {
            $writer = Writer::findOrFail($id);
            $writer->delete();
            return response()->json(['message' => 'writer successfully deleted!'], 200);
        } catch (\Exception $e) {
            return response()->json(['message' => 'Unfortunately writer was not deleted!'], 400);
        }
    }
}
