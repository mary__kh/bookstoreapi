<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Mail;
use App\Mail\RegisterConfirmation;
use App\Http\Controllers\Traits\ImageUpload;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Carbon;

class AuthController extends Controller
{
    use ImageUpload;
    public function create(Request $request)
    {
        //validate incoming request
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|min:3|max:255',
            'lastname' => 'required|string|min:3|max:255',
            'email' => 'required|email|unique:users',
            'gender' => 'required|in:male,female',
            'age' => 'required|numeric',
            'avatar' => 'nullable|image|mimes:jpeg,jpg,png,gif|max:100000',
            'birth_date' => 'required|date_format:Y-m-d',
            'password' => 'nullable|confirmed',
        ]);
        $data = $validator->validated();
        try {
            if ($request->get('password') == "") {
                $data['password'] = app('hash')->make('12345678');
            } else {
                $data['password'] = app('hash')->make($request->get('password'));
            }
            $data['education_id'] = $request->education_id;
            $user = User::create($data);
            if ($request->file('avatar') != null) {
                $user->avatar = $this->saveImages($data['avatar']);
            } else {
                $user->avatar = null;
            }
            Mail::to($data['email'])->send(new RegisterConfirmation());
            $user->register_at = Carbon::now()->toDateTimeString();
            $user->save();
            //return successful response
            return response()->json(['user' => $user, 'message' => 'CREATED'], 201);
        } catch (\Exception $e) {
            //return error message
            return response()->json(['message' => 'User Registration Failed!'], 409);
        }
    }
}
