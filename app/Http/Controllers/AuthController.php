<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Http\Controllers\Traits\ImageUpload;
use Illuminate\Support\Facades\Validator;
use App\Mail\RegisterConfirmation;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Carbon;


class AuthController extends Controller
{
    use ImageUpload;
    /**
     * Store a new user.
     *
     * @param  Request  $request
     * @return Response
     */
    public function register(Request $request)
    {
        //validate incoming request
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|min:3|max:255',
            'lastname' => 'required|string|min:3|max:255',
            'email' => 'required|email|unique:users',
            'gender' => 'required|in:male,female',
            'age' => 'required|numeric',
            'avatar' => 'nullable|image|mimes:jpeg,jpg,png,gif|max:100000',
            'birth_date' => 'required|date_format:Y-m-d',
            'password' => 'required|confirmed',
        ]);
        $data = $validator->validated();
        try {
            $data['password'] = app('hash')->make($request->get('password'));
            $data['education_id'] = $request->education_id;
            $user = User::create($data);
            if ($request->file('avatar') != null) {
                $user->avatar = $this->saveImages($data['avatar']);
            } else {
                $user->avatar = null;
            }
            Mail::to($data['email'])->send(new RegisterConfirmation());
            $user->register_at = Carbon::now()->toDateTimeString();
            $user->save();
            //return successful response
            return response()->json(['user' => $user, 'message' => 'CREATED'], 201);
        } catch (\Exception $e) {
            //return error message
            return response()->json(['message' => 'User Registration Failed!'], 409);
        }
    }
    /**
     * Get a JWT via given credentials.
     *
     * @param  Request  $request
     * @return Response
     */
    public function login(Request $request)
    {
          //validate incoming request
        $this->validate($request, [
            'email' => 'required|string',
            'password' => 'required|string',
        ]);

        $credentials = $request->only(['email', 'password']);

        if (! $token = Auth::attempt($credentials)) {
            return response()->json(['message' => 'Unauthorized'], 401);
        }

        return $this->respondWithToken($token);
    }
}
