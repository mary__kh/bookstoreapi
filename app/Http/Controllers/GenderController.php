<?php

namespace App\Http\Controllers;
use App\Repository\Genders as RepositoryGenders;


use Illuminate\Http\Request;

class GenderController extends Controller
{
    public function index(){
        $info = new RepositoryGenders;
        $info = $info->all('id');
        return $info;
    }
}
