<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Traits\ImageUpload;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use App\Libraries\Helpers;

class UserController extends Controller
{
    use ImageUpload;
    /**
     * Instantiate a new UserController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Get the authenticated User.
     *
     * @return Response
     */
    public function profile()
    {
        return response()->json(['user' => Auth::user()], 200);
    }

    /**
     * Get all User.
     *
     * @return Response
     */
    public function allUsers()
    {
        return response()->json(['users' =>  User::all()], 200);
    }

    /**
     * Get one user.
     *
     * @return Response
     */
    public function singleUser($id)
    {
        try {
            $user = User::findOrFail($id);

            return response()->json(['user' => $user], 200);
        } catch (\Exception $e) {

            return response()->json(['message' => 'user not found!'], 404);
        }
    }
    /**
     * Update one user.
     *
     * @return Response
     */
    public function updateUser($id, Request $request)
    {

        $user = User::findOrFail($id);
        $validator = Validator::make($request->all(), [
            'name' => 'string|min:3|max:255',
            'lastname' => 'string|min:3|max:255',
            'email' => 'email|unique:users',
            'gender' => 'in:male,female',
            'age' => 'numeric',
            'avatar' => 'nullable|image|mimes:jpeg,jpg,png,gif|max:100000',
            'birth_date' => 'date_format:Y-m-d',
            'password' => 'confirmed',
        ]);
        $data = $validator->validated();
        try {
            $data['password'] = app('hash')->make($request->get('password'));
            if ($request->hasFile('avatar')) {
                $destination = Helpers::public_path().'/'.$user->avatar;
                if(file_exists($destination)){
                    File::delete($destination);
                }
                $user->avatar = $this->saveImages($data['avatar']);
            }
            $data['avatar'] = $user->avatar;
            $data['education_id'] = $request->education_id;
            $user->update($data);
            return response()->json(['message' => 'user updated successfully!'], 200);
        } catch (\Exception $e) {

            return response()->json(['message' => 'unfortunately user was not updated!'], 403);
        }
    }
    /**
     * delete one user.
     *
     * @return Response
     */
    public function deleteUser($id)
    {
        try {
            $user = User::findOrFail($id);
            $user->delete();
            return response()->json(['message' => 'User successfully deleted!'], 200);
        } catch (\Exception $e) {
            return response()->json(['message' => 'Unfortunately user was not deleted!'], 400);
        }
    }
}
