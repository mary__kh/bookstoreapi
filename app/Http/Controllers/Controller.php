<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Carbon;

class Controller extends BaseController
{
    public function authenticated($user)
    {
        $user->login_at = Carbon::now()->toDateTimeString();
        $user->save();
    }
    protected function respondWithToken($token)
    {
        $this->authenticated(Auth::user());
        return response()->json([
            'token' => $token,
            'token_type' => 'bearer',
            'expires_in' => Auth::factory()->getTTL() * 60
        ], 200);
    }
}
