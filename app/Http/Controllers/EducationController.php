<?php

namespace App\Http\Controllers;

use App\Models\Education;
use App\Repository\Educations as RepositoryEducations;
use Illuminate\Http\Request;

class EducationController extends Controller
{
    public function index(){
        $info = new RepositoryEducations;
        $info = $info->all('id');
        return $info;
    }
}
